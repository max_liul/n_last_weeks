import pandas as pd


def date_to_unit_of_time(data_frame, date_column_name, normalized_date_column_name,
                    unit_of_time='seconds'):  # unit_of_time = 'seconds' or 'days'

    data_frame[date_column_name] = pd.to_datetime(data_frame[date_column_name])
    minimum_date = data_frame[date_column_name].min()
    if unit_of_time == 'seconds':
        data_frame[normalized_date_column_name] = data_frame[date_column_name].apply(
            lambda date: (date - minimum_date).seconds + (24 * 60 * 60) * (date - minimum_date).days)
    elif unit_of_time == 'days':
        data_frame[normalized_date_column_name] = data_frame[date_column_name].apply(
            lambda date: (date - minimum_date).days)
    elif unit_of_time == 'weeks':
        data_frame[normalized_date_column_name] = data_frame[date_column_name].apply(
            lambda date: (date - minimum_date).days // 7)
    else:
        return print('Not supported value of time units')
    return data_frame


def last_n_weeks_selected(path_to_dataframe_in_csv,
                          date_column,
                          transaction_column,
                          # if there is no a transaction column in the df then put transaction_column = 'index'
                          column_for_analysis_name,
                          number_of_weeks,
                          # it indicates number of weeks (int), sterting from the end of df, which will be considered
                          product_column='StockCode', user_column='CustomerID',
                          quantity_column='Product Quantity',
                          is_normalized=True,
                          output_file=None,
                          consider_last_and_previous_weeks=True):  # if consider_last_and_previous_weeks = True then n last + n previous weeks will be considered

    df = pd.read_csv(path_to_dataframe_in_csv)

    # if there is no a transaction column in the df then we create it and assign an index value to the transaction column
    if transaction_column == 'index':
        transaction_column = 'transaction_id'
        df[transaction_column] = df.index

    df[date_column] = pd.to_datetime(df[date_column])

    # select a part of df where the first week starts from Monday and the last week fineshes by Sunday
    start_index_of_df_selected = df.loc[
        df[date_column].dt.day_name() == 'Monday'].first_valid_index()
    finish_index_of_df_selected = df.loc[
        df[date_column].dt.day_name() == 'Sunday'].last_valid_index()

    df_selected = df.loc[(df.index >= start_index_of_df_selected) & (
            df.index < finish_index_of_df_selected)].copy()

    df_selected_with_normalized_date = date_to_unit_of_time(df_selected, date_column,
                                                                            'Normalized Date',
                                                                            unit_of_time='weeks')

    # Creation of the column 'Weeks Marker' and putting values from 0 to number_of_weeks to them.
    max_week = df_selected_with_normalized_date['Normalized Date'].max()
    df_selected_with_normalized_date.loc[
        df_selected_with_normalized_date['Normalized Date'] > max_week - number_of_weeks, 'Weeks Marker'] = \
        df_selected_with_normalized_date['Normalized Date'].apply(
            lambda week_number: '{} last'.format(week_number - max_week + number_of_weeks - 1))

    if consider_last_and_previous_weeks:
        df_selected_with_normalized_date.loc[
            (df_selected_with_normalized_date['Normalized Date'] > max_week - 2 * number_of_weeks) & (
                    df_selected_with_normalized_date[
                        'Normalized Date'] <= max_week - number_of_weeks), 'Weeks Marker'] = \
            df_selected_with_normalized_date['Normalized Date'].apply(
                lambda week_number: '{} previous'.format(week_number - max_week + 2*number_of_weeks - 1))

    list_of_selected_columns = [column_for_analysis_name, transaction_column, quantity_column, 'Weeks Marker']
    df = df_selected_with_normalized_date[list_of_selected_columns].copy()

    sorted_columns = df_selected_with_normalized_date['Weeks Marker'].unique()[1:]

    if column_for_analysis_name == product_column:
        df_grouped_by_column_for_analysis_and_week = df.groupby([column_for_analysis_name, 'Weeks Marker'])[
            quantity_column].sum().unstack().fillna(0)
    elif column_for_analysis_name == user_column:

        df_grouped_by_column_for_analysis_time_interval_transaction = df.groupby(
            [column_for_analysis_name, transaction_column, 'Weeks Marker'], as_index=False).count()

        # counting the number of visits for each user for a considered day of the week
        df_grouped_by_column_for_analysis_and_week = \
            df_grouped_by_column_for_analysis_time_interval_transaction.groupby(
                [column_for_analysis_name, 'Weeks Marker'])[transaction_column].count().unstack().fillna(0)
    else:
        return print('column_for_analysis_name value is not correct')

    # reordering of the columns
    df_grouped_by_column_for_analysis_and_week = df_grouped_by_column_for_analysis_and_week[sorted_columns]

    # create a list of columns' name (0th column is a column_for_analysis, so we must exclude it from the list)
    df_grouped_by_column_for_analysis_and_week['Total'] = df_grouped_by_column_for_analysis_and_week.sum(axis=1)

    # normalization: create a list of days of the week (columns of a df_grouped_by_column_for_analysis_time_interval) and
    # iterate over them, dividing on a total amount of visits/quantities
    if is_normalized:
        for marker in sorted_columns:
            df_grouped_by_column_for_analysis_and_week[marker] = df_grouped_by_column_for_analysis_and_week[
                                                                     marker] / \
                                                                 df_grouped_by_column_for_analysis_and_week['Total']

    if output_file is not None:
        return df_grouped_by_column_for_analysis_and_week.sort_values('Total',
                                                                      ascending=False).reset_index().to_csv(
            '{}.csv'.format(output_file), index=False)
    return df_grouped_by_column_for_analysis_and_week.reset_index()


last_n_weeks_selected('kauia_dataset_excluded_extras.csv', 'Transaction Date', 'Transaction ID', 'Product Name', 4,
                  product_column='Product Name', user_column='Member ID', output_file='Product Name_selected_weeks_4',
                      consider_last_and_previous_weeks=True)

# last_n_weeks_selected(path_to_dataframe_in_csv,
#                           date_column,
#                           transaction_column,
#                           # if there is no a transaction column in the df then put transaction_column = 'index'
#                           column_for_analysis_name,
#                           number_of_weeks,
#                           # it indicates number of weeks (int), sterting from the end of df, which will be considered
#                           product_column='StockCode', user_column='CustomerID',
#                           quantity_column='Product Quantity',
#                           is_normalized=True,
#                           output_file=None,
#                           consider_last_and_previous_weeks=True)

# date_to_seconds('kauia_dataset_excluded_extras.csv', 'Transaction Date', 'Normalized Date', unit_of_time='weeks').to_csv('weeks.csv')